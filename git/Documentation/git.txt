git(7)
======

NAME
----
git - the stupid content tracker


SYNOPSIS
--------
[verse]
'git' [--version] [--exec-path[=GIT_EXEC_PATH]] [-p|--paginate]
    [--bare] [--git-dir=GIT_DIR] [--work-tree=GIT_WORK_TREE]
    [--help] COMMAND [ARGS]

DESCRIPTION
-----------
Git is a fast, scalable, distributed revision control system with an
unusually rich command set that provides both high-level operations
and full access to internals.

See this link:tutorial.html[tutorial] to get started, then see
link:everyday.html[Everyday Git] for a useful minimum set of commands, and
"man git-commandname" for documentation of each command.  CVS users may
also want to read link:cvs-migration.html[CVS migration].  See
link:user-manual.html[Git User's Manual] for a more in-depth
introduction.

The COMMAND is either a name of a Git command (see below) or an alias
as defined in the configuration file (see gitlink:git-config[1]).

Formatted and hyperlinked version of the latest git
documentation can be viewed at
`http://www.kernel.org/pub/software/scm/git/docs/`.

ifdef::stalenotes[]
[NOTE]
============

You are reading the documentation for the latest (possibly
unreleased) version of git, that is available from 'master'
branch of the `git.git` repository.
Documentation for older releases are available here:

* link:v1.5.2.4/git.html[documentation for release 1.5.2.4]

* release notes for
  link:RelNotes-1.5.2.4.txt[1.5.2.4],
  link:RelNotes-1.5.2.3.txt[1.5.2.3],
  link:RelNotes-1.5.2.2.txt[1.5.2.2],
  link:RelNotes-1.5.2.1.txt[1.5.2.1],
  link:RelNotes-1.5.2.txt[1.5.2].

* link:v1.5.1.6/git.html[documentation for release 1.5.1.6]

* release notes for
  link:RelNotes-1.5.1.6.txt[1.5.1.6],
  link:RelNotes-1.5.1.5.txt[1.5.1.5],
  link:RelNotes-1.5.1.4.txt[1.5.1.4],
  link:RelNotes-1.5.1.3.txt[1.5.1.3],
  link:RelNotes-1.5.1.2.txt[1.5.1.2],
  link:RelNotes-1.5.1.1.txt[1.5.1.1],
  link:RelNotes-1.5.1.txt[1.5.1].

* link:v1.5.0.7/git.html[documentation for release 1.5.0.7]

* release notes for
  link:RelNotes-1.5.0.7.txt[1.5.0.7],
  link:RelNotes-1.5.0.6.txt[1.5.0.6],
  link:RelNotes-1.5.0.5.txt[1.5.0.5],
  link:RelNotes-1.5.0.3.txt[1.5.0.3],
  link:RelNotes-1.5.0.2.txt[1.5.0.2],
  link:RelNotes-1.5.0.1.txt[1.5.0.1],
  link:RelNotes-1.5.0.txt[1.5.0].

* documentation for release link:v1.4.4.4/git.html[1.4.4.4],
  link:v1.3.3/git.html[1.3.3],
  link:v1.2.6/git.html[1.2.6],
  link:v1.0.13/git.html[1.0.13].

============

endif::stalenotes[]

OPTIONS
-------
--version::
	Prints the git suite version that the 'git' program came from.

--help::
	Prints the synopsis and a list of the most commonly used
	commands.  If a git command is named this option will bring up
	the man-page for that command. If the option '--all' or '-a' is
	given then all available commands are printed.

--exec-path::
	Path to wherever your core git programs are installed.
	This can also be controlled by setting the GIT_EXEC_PATH
	environment variable. If no path is given 'git' will print
	the current setting and then exit.

-p|--paginate::
	Pipe all output into 'less' (or if set, $PAGER).

--git-dir=<path>::
	Set the path to the repository. This can also be controlled by
	setting the GIT_DIR environment variable.

--work-tree=<path>::
	Set the path to the working tree.  The value will not be
	used in combination with repositories found automatically in
	a .git directory (i.e. $GIT_DIR is not set).
	This can also be controlled by setting the GIT_WORK_TREE
	environment variable and the core.worktree configuration
	variable.

--bare::
	Same as --git-dir=`pwd`.

FURTHER DOCUMENTATION
---------------------

See the references above to get started using git.  The following is
probably more detail than necessary for a first-time user.

The <<Discussion,Discussion>> section below and the
link:core-tutorial.html[Core tutorial] both provide introductions to the
underlying git architecture.

See also the link:howto-index.html[howto] documents for some useful
examples.

GIT COMMANDS
------------

We divide git into high level ("porcelain") commands and low level
("plumbing") commands.

High-level commands (porcelain)
-------------------------------

We separate the porcelain commands into the main commands and some
ancillary user utilities.

Main porcelain commands
~~~~~~~~~~~~~~~~~~~~~~~

include::cmds-mainporcelain.txt[]

Ancillary Commands
~~~~~~~~~~~~~~~~~~
Manipulators:

include::cmds-ancillarymanipulators.txt[]

Interrogators:

include::cmds-ancillaryinterrogators.txt[]


Interacting with Others
~~~~~~~~~~~~~~~~~~~~~~~

These commands are to interact with foreign SCM and with other
people via patch over e-mail.

include::cmds-foreignscminterface.txt[]


Low-level commands (plumbing)
-----------------------------

Although git includes its
own porcelain layer, its low-level commands are sufficient to support
development of alternative porcelains.  Developers of such porcelains
might start by reading about gitlink:git-update-index[1] and
gitlink:git-read-tree[1].

The interface (input, output, set of options and the semantics)
to these low-level commands are meant to be a lot more stable
than Porcelain level commands, because these commands are
primarily for scripted use.  The interface to Porcelain commands
on the other hand are subject to change in order to improve the
end user experience.

The following description divides
the low-level commands into commands that manipulate objects (in
the repository, index, and working tree), commands that interrogate and
compare objects, and commands that move objects and references between
repositories.


Manipulation commands
~~~~~~~~~~~~~~~~~~~~~

include::cmds-plumbingmanipulators.txt[]


Interrogation commands
~~~~~~~~~~~~~~~~~~~~~~

include::cmds-plumbinginterrogators.txt[]

In general, the interrogate commands do not touch the files in
the working tree.


Synching repositories
~~~~~~~~~~~~~~~~~~~~~

include::cmds-synchingrepositories.txt[]

The following are helper programs used by the above; end users
typically do not use them directly.

include::cmds-synchelpers.txt[]


Internal helper commands
~~~~~~~~~~~~~~~~~~~~~~~~

These are internal helper commands used by other commands; end
users typically do not use them directly.

include::cmds-purehelpers.txt[]


Configuration Mechanism
-----------------------

Starting from 0.99.9 (actually mid 0.99.8.GIT), `.git/config` file
is used to hold per-repository configuration options.  It is a
simple text file modeled after `.ini` format familiar to some
people.  Here is an example:

------------
#
# A '#' or ';' character indicates a comment.
#

; core variables
[core]
	; Don't trust file modes
	filemode = false

; user identity
[user]
	name = "Junio C Hamano"
	email = "junkio@twinsun.com"

------------

Various commands read from the configuration file and adjust
their operation accordingly.


Identifier Terminology
----------------------
<object>::
	Indicates the object name for any type of object.

<blob>::
	Indicates a blob object name.

<tree>::
	Indicates a tree object name.

<commit>::
	Indicates a commit object name.

<tree-ish>::
	Indicates a tree, commit or tag object name.  A
	command that takes a <tree-ish> argument ultimately wants to
	operate on a <tree> object but automatically dereferences
	<commit> and <tag> objects that point at a <tree>.

<commit-ish>::
	Indicates a commit or tag object name.  A
	command that takes a <commit-ish> argument ultimately wants to
	operate on a <commit> object but automatically dereferences
	<tag> objects that point at a <commit>.

<type>::
	Indicates that an object type is required.
	Currently one of: `blob`, `tree`, `commit`, or `tag`.

<file>::
	Indicates a filename - almost always relative to the
	root of the tree structure `GIT_INDEX_FILE` describes.

Symbolic Identifiers
--------------------
Any git command accepting any <object> can also use the following
symbolic notation:

HEAD::
	indicates the head of the current branch (i.e. the
	contents of `$GIT_DIR/HEAD`).

<tag>::
	a valid tag 'name'
	(i.e. the contents of `$GIT_DIR/refs/tags/<tag>`).

<head>::
	a valid head 'name'
	(i.e. the contents of `$GIT_DIR/refs/heads/<head>`).

For a more complete list of ways to spell object names, see
"SPECIFYING REVISIONS" section in gitlink:git-rev-parse[1].


File/Directory Structure
------------------------

Please see link:repository-layout.html[repository layout] document.

Read link:hooks.html[hooks] for more details about each hook.

Higher level SCMs may provide and manage additional information in the
`$GIT_DIR`.


Terminology
-----------
Please see link:glossary.html[glossary] document.


Environment Variables
---------------------
Various git commands use the following environment variables:

The git Repository
~~~~~~~~~~~~~~~~~~
These environment variables apply to 'all' core git commands. Nb: it
is worth noting that they may be used/overridden by SCMS sitting above
git so take care if using Cogito etc.

'GIT_INDEX_FILE'::
	This environment allows the specification of an alternate
	index file. If not specified, the default of `$GIT_DIR/index`
	is used.

'GIT_OBJECT_DIRECTORY'::
	If the object storage directory is specified via this
	environment variable then the sha1 directories are created
	underneath - otherwise the default `$GIT_DIR/objects`
	directory is used.

'GIT_ALTERNATE_OBJECT_DIRECTORIES'::
	Due to the immutable nature of git objects, old objects can be
	archived into shared, read-only directories. This variable
	specifies a ":" separated list of git object directories which
	can be used to search for git objects. New objects will not be
	written to these directories.

'GIT_DIR'::
	If the 'GIT_DIR' environment variable is set then it
	specifies a path to use instead of the default `.git`
	for the base of the repository.

'GIT_WORK_TREE'::
	Set the path to the working tree.  The value will not be
	used in combination with repositories found automatically in
	a .git directory (i.e. $GIT_DIR is not set).
	This can also be controlled by the '--work-tree' command line
	option and the core.worktree configuration variable.

git Commits
~~~~~~~~~~~
'GIT_AUTHOR_NAME'::
'GIT_AUTHOR_EMAIL'::
'GIT_AUTHOR_DATE'::
'GIT_COMMITTER_NAME'::
'GIT_COMMITTER_EMAIL'::
'GIT_COMMITTER_DATE'::
'EMAIL'::
	see gitlink:git-commit-tree[1]

git Diffs
~~~~~~~~~
'GIT_DIFF_OPTS'::
	Only valid setting is "--unified=??" or "-u??" to set the
	number of context lines shown when a unified diff is created.
	This takes precedence over any "-U" or "--unified" option
	value passed on the git diff command line.

'GIT_EXTERNAL_DIFF'::
	When the environment variable 'GIT_EXTERNAL_DIFF' is set, the
	program named by it is called, instead of the diff invocation
	described above.  For a path that is added, removed, or modified,
        'GIT_EXTERNAL_DIFF' is called with 7 parameters:

	path old-file old-hex old-mode new-file new-hex new-mode
+
where:

	<old|new>-file:: are files GIT_EXTERNAL_DIFF can use to read the
                         contents of <old|new>,
	<old|new>-hex:: are the 40-hexdigit SHA1 hashes,
	<old|new>-mode:: are the octal representation of the file modes.

+
The file parameters can point at the user's working file
(e.g. `new-file` in "git-diff-files"), `/dev/null` (e.g. `old-file`
when a new file is added), or a temporary file (e.g. `old-file` in the
index).  'GIT_EXTERNAL_DIFF' should not worry about unlinking the
temporary file --- it is removed when 'GIT_EXTERNAL_DIFF' exits.
+
For a path that is unmerged, 'GIT_EXTERNAL_DIFF' is called with 1
parameter, <path>.

other
~~~~~
'GIT_MERGE_VERBOSITY'::
	A number controlling the amount of output shown by
	the recursive merge strategy.  Overrides merge.verbosity.
	See gitlink:git-merge[1]

'GIT_PAGER'::
	This environment variable overrides `$PAGER`.

'GIT_FLUSH'::
	If this environment variable is set to "1", then commands such
	as git-blame (in incremental mode), git-rev-list, git-log,
	git-whatchanged, etc., will force a flush of the output stream
	after each commit-oriented record have been flushed.   If this
	variable is set to "0", the output of these commands will be done
	using completely buffered I/O.   If this environment variable is
	not set, git will choose buffered or record-oriented flushing
	based on whether stdout appears to be redirected to a file or not.

'GIT_TRACE'::
	If this variable is set to "1", "2" or "true" (comparison
	is case insensitive), git will print `trace:` messages on
	stderr telling about alias expansion, built-in command
	execution and external command execution.
	If this variable is set to an integer value greater than 1
	and lower than 10 (strictly) then git will interpret this
	value as an open file descriptor and will try to write the
	trace messages into this file descriptor.
	Alternatively, if this variable is set to an absolute path
	(starting with a '/' character), git will interpret this
	as a file path and will try to write the trace messages
	into it.

Discussion[[Discussion]]
------------------------
include::core-intro.txt[]

Authors
-------
* git's founding father is Linus Torvalds <torvalds@osdl.org>.
* The current git nurse is Junio C Hamano <junkio@cox.net>.
* The git potty was written by Andres Ericsson <ae@op5.se>.
* General upbringing is handled by the git-list <git@vger.kernel.org>.

Documentation
--------------
The documentation for git suite was started by David Greaves
<david@dgreaves.com>, and later enhanced greatly by the
contributors on the git-list <git@vger.kernel.org>.

GIT
---
Part of the gitlink:git[7] suite
